import ast
import base64

from Crypto.Util.Padding import unpad
from Crypto.Cipher import AES
from Crypto.Cipher import ChaCha20_Poly1305
from binascii import hexlify
from base64 import b64decode, decodebytes
import funcy
import hashlib
import os


class Function():
    """
        The functions inside this class need to be fixed. They are listed in order for the process to work.
    """

    def __init__(self):
        self.path = os.path.dirname(os.path.realpath(__file__))
        self.testImagePath = os.path.join(self.path, "..", "static", "images", "test.png")
        self.encryptedImagePath = os.path.join(self.path, "..", "static", "images", "encrypted.bin")
        self.useless = funcy.chunks(2, "This is a useless line")
        self.decryption_password = b'1-2-4-5-8-1000-1001'

    def prepare_image(self):
        try:
            print(self.testImagePath)
            self.delete_file(self.testImagePath)  # Cleanup function
            file_bytes = self.decrypt_image_file(self.encryptedImagePath)
            self.encode_and_save_image(file_bytes)
        except Exception as e:
            print("----------------------------------------------------------------")
            print("The image could not be prepared, did the decrypt process failed?")
            print("----------------------------------------------------------------")

    def delete_file(self, filename):
        print('*******************')
        print(os.path.exists(filename))
        if os.path.exists(filename):
            os.remove(filename)

    def decrypt_image_file(self, filename):
        """
        The image was encrypted using ChaCha20 and its MAC (Poly1305)
        Now that you have the secret_key use it to decrypt the image!
        """
        hashed_secrey_key = self.get_secret_key()

        with open(filename, "rb") as f:
            filedata = f.read()
        filedata = filedata.decode("utf-8")

        # The 'filedata' variable is now a string which needs to be transformed into a dict object by modifying the next line
        b64_encryption_dict = eval(filedata)
        dict_elements = ['nonce', 'header', 'ciphertext', 'tag']
        encryption_dict = {item: b64decode(b64_encryption_dict[item]) for item in dict_elements}

        # Replace the next line with the correct code. You will need the secret_key, ciphertext, nonce, header and tag, available from the 'encryption_dict'
        cipher = ChaCha20_Poly1305.new(key=hashed_secrey_key, nonce=encryption_dict['nonce'])
        cipher.update(encryption_dict['header'])
        image_file_bytes = cipher.decrypt_and_verify(encryption_dict['ciphertext'], encryption_dict['tag'])

        return image_file_bytes

    def decrypt_binary_file_with_ecb(self, filename) -> bytes:
        actual_key = self.decryption_password[-16:]
        with open(filename, "rb") as f:
            cipher_data = f.read()

        # The 'cipher_data' need to be decrypted into 'decrypted_data'
        # Using the AES Algorithm in ECB Mode, use the 'actual_key' to decrypt your file
        cipher = AES.new(actual_key, AES.MODE_ECB)
        # TODO: get ciphertext of the cipher_data
        # new_cipher = ast.literal_eval(cipher_data.decode('utf-8'))
        # ciphertext = base64.decodebytes(new_cipher['ciphertext'].encode(encoding='utf-8'))
        decrypted_data = cipher.decrypt(cipher_data)

        # For the unpadding we are using this library as an example:
        # "Using the Default ECB Block size! This should be an integer"
        defined_block_size = AES.block_size
        file_bytes = unpad(b64decode(decrypted_data), defined_block_size)
        return file_bytes

    def get_secret_key(self) -> bytes:
        """
        To get the secret key you need to call the 'decrypt_binary_file_with_ecb' with the correct file name
        The key is of binary type.
        """
        return self.hash_secret_key(self.decrypt_binary_file_with_ecb(self.encryptedImagePath))

    def hash_secret_key(self, bytes_key):
        """
        The actual decryption of the image_file that is displayed on the browser is done with a hashed key.
        """

        salt = b'0d9165b75075fbbbee44d2bf0e7f1ed500099917968ad2a80efbfe2266e76652'
        iteration = 322
        # Create a hash (SHA-2 type, with a 256 value) using the PBKDF2-HMAC standard.
        # For this you will need:
        # The bytes_key that will be hashed
        # The salt and iterations variable
        # Replace the following line with the appropiate code for this function to work!
        hashed_bytes_key = hashlib.pbkdf2_hmac('sha256', bytes_key, salt, iteration)

        hashed_secret_key = hexlify(hashed_bytes_key)
        return hashed_secret_key[-32:]

    def encode_and_save_image(self, image_file_bytes):
        with open(self.testImagePath, 'wb') as image:
            # The file_bytes need to be decoded from Base 64 to regular bytes and written into a file.
            decodedB64 = decodebytes(image_file_bytes)
            image.write(decodedB64)
