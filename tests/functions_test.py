import unittest
from resources.functions import Function


class MyTestCase(unittest.TestCase):

    def test_decrypt_image_file(self):
        funcions = Function()
        bytes = funcions.decrypt_image_file(funcions.encryptedImagePath)
        print(bytes)

        self.assertIsNotNone(bytes.read())


if __name__ == '__main__':
    unittest.main()
