Description:
This application shows an image through your web browser.

Requirements: 
-Python 3.6 o higher (any operating system)
-pip

Install:

```
$ sudo dnf install python3-virtualenv
$ virtualenv-3 env
$ source ./env/bin/activate
$ pip install -r requirements.txt
```

- To install the needed dependencies use the following command on your console:
cd <route where you extracted this test>/this_python_test/
pip install -r requirements.txt

Run it:
cd <route where you extracted this test>/this_python_test/
python app.py

Use:
Go to http://127.0.0.1:5000 in your browser.

Challenge:
This application is not working properly in its current state, follow the comments throughout the source code to fix it.

Help:
You need to obtain a decryption key, which is secured inside this proyect under other encryption methods.
The First Key, called "decryption_password" is given to you here:

Resolver este acertijo!
Completar la siguiente sucesión númerica (reemplazar la letra x con el numero correcto)
'1-2-4-5-8-1000-x'

*Esta cadena incluyendo los '-' es la 'decryption_password'
