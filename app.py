from flask import Flask, render_template
from resources.functions import Function
import os

static_path=os.path.join(os.path.dirname(os.path.realpath(__file__)),'static')
app=Flask(__name__, template_folder = static_path, static_folder = static_path)

@app.route('/')
@app.route('/index')
def show_index():
    # The 'prepare_image' function readies the image to be displayed in your browser.
    Function().prepare_image()
    return render_template("index.html")


if __name__ == '__main__':
    app.run()
